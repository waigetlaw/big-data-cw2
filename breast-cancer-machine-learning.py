import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn import metrics

pd.set_option('display.max_columns', 15)

# Read data
df = pd.read_csv("breast-cancer-wisconsin.data", header=None)
print "dataframe shape: ", df.shape
# print df.describe()

# Data Pre-processing

# drop duplicates (including id attribute)
duplicate_rows_df = df[df.duplicated()]
print "number of duplicate rows: ", duplicate_rows_df.shape
df = df.drop_duplicates()

# drop instances with missing data
print "attributes with missing data: "
print (df == "?").sum()

# only attribute 6 has missing data, represented as ?
df = df.drop(df[df[6] == "?"].index)

# drop id
df = df.drop(0, axis=1)

X = df.drop(10, axis=1)
Y = df[10]

print "\nProcessed Data\n=============="
print "Y: ", Y.shape
print "X: ", X.shape


# [BEGIN BAR CHART]
benign = (df[10] == 2).sum()
malignant = (df[10] == 4).sum()

class_bar = [benign, malignant]
labels = ['Benign', 'Malignant']
bar_x = np.arange(len(labels))

fig, ax = plt.subplots()

rects = ax.bar(bar_x, class_bar, label='Cancer Type')

ax.set_ylabel('Number of instances')
ax.set_title('Breast cancer instances separated (Post Process)')
ax.set_xticks(bar_x)
ax.set_xticklabels(labels)


def autolabel(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


autolabel(rects)

plt.ylim(0, 500)
plt.show()

# [END OF BAR CHART]

# Train
X_train, X_test, y_train, y_test = train_test_split(
    X, Y, test_size=0.2, random_state=1)

depth_array = []
acc_array = []

# Results for different depths
for depth in range(5, 6):  # only range 5, 6 for image generation, normally 1, 15

    clf = DecisionTreeClassifier(
        max_depth=depth,
        min_samples_leaf=3
    )

    no_runs = 1  # only 1 for image generation, normally 100
    acc = 0

    for i in range(no_runs):
        clf = clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        acc += metrics.accuracy_score(y_test, y_pred)

    depth_array.append(depth)
    acc_array.append(np.round(acc/no_runs, 3))

    # Generate Decision Tree Image
    from sklearn.tree import export_graphviz
    from sklearn.externals.six import StringIO
    from IPython.display import Image
    import pydotplus

    feature_cols = ['clump thickness', 'uniformity of cell size', 'uniformity of cell shape', 'marginal adhesion', 'single epithelial cell size',
                    'bare nuclei', 'bland chromatin', 'normal nucleoli', 'mitoses']
    dot_data = StringIO()
    export_graphviz(clf, out_file=dot_data,
                    filled=True, rounded=True,
                    special_characters=True,
                    feature_names=feature_cols,
                    class_names=['0', '1'])

    graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
    graph.write_png('breast-cancer.png')
    Image(graph.create_png())

fig, ax = plt.subplots()

ymax = max(acc_array)
xpos = acc_array.index(ymax)
xmax = depth_array[xpos]

line, = ax.plot(depth_array, acc_array)

ax.annotate('Max: (depth = {}, acc = {})'.format(xpos, ymax), xy=(xmax, ymax),
            xytext=(xmax, ymax), arrowprops=dict(facecolor='black', shrink=0.05))

ax.set_ylabel('Accuracy')
ax.set_xlabel('Max Depth')
ax.set_title('Accuracy vs Max Depth')

plt.xticks(depth_array)
plt.show()
